<?php

function sedc_post_types(){
    register_post_type('event', array(
        'has_archive' => true,
        'rewrite' => array(
            'slug' => 'events'
        ),
        'public' => true,
        'labels' => array(
            'name' => 'Events',
            'add_new_item' => 'Add New Event',
            'edit_item' => 'Edit Event',
            'all_items' => 'All Events',
            'singluar_name' => 'Event'
        ),
        'menu_icon' => 'dashicons-calendar-alt'
    ));
}

add_action('init', 'sedc_post_types');