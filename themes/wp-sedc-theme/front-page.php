<?php get_header(); ?>

    <div class="page-banner">
      <div class="page-banner__bg-image" style="background-image: url( <?php echo get_theme_file_uri('/images/academies-web.png'); ?> )"></div>
      <div class="page-banner__content container t-center c-white">
        <h1 class="headline headline--large">Seavus education and development centre</h1>
        <a href="#" class="btn btn--large btn--blue">Explore programs</a>
      </div>
    </div>

    <div class="full-width-split group">
      <div class="full-width-split__one">
        <div class="full-width-split__inner">
          <h2 class="headline headline--small-plus t-center">Upcoming Events</h2>
          <?php 
            $homepageEvents = new WP_Query(array(
              'posts_per_page' => 2,
              'post_type' => 'event'
            ));

            while($homepageEvents->have_posts()){
              $homepageEvents->the_post(); ?>            
              <div class="event-summary">
                <a class="event-summary__date t-center" href="<?php the_permalink(); ?>">
                  <span class="event-summary__month"><?php the_time('M'); ?></span>
                  <span class="event-summary__day"><?php the_time('d'); ?></span>
                </a>
                <div class="event-summary__content">
                  <h5 class="event-summary__title headline headline--tiny"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                  <p><?php echo wp_trim_words(get_the_content(), 12); ?> <a href="<?php the_permalink(); ?>" class="nu gray">Attend</a></p>
                </div>
              </div>
            <?php } wp_reset_postdata(); ?>
          <p class="t-center no-margin"><a href="<?php echo get_post_type_archive_link('event'); ?>" class="btn btn--blue">View All Events</a></p>
        </div>
      </div>
      <div class="full-width-split__two">
        <div class="full-width-split__inner">
          <h2 class="headline headline--small-plus t-center">From Our News Category</h2>
            <?php 
                $homeQryPosts = new WP_Query(array(
                    'posts_per_page' => 2,
                    'category_name' => 'news',
                ));
                while($homeQryPosts->have_posts()){
                $homeQryPosts->the_post(); ?>
                    <div class="event-summary">
                        <a class="event-summary__date event-summary__date--beige t-center" href="<?php the_permalink(); ?>">
                        <span class="event-summary__month"><?php the_time('M'); ?></span>
                        <span class="event-summary__day"><?php the_time('d'); ?></span>
                        </a>
                        <div class="event-summary__content">
                        <h5 class="event-summary__title headline headline--tiny"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                        <p><?php echo wp_trim_words(get_the_content(), 12); ?><a href="<?php the_permalink(); ?>" class="nu gray">Read more</a></p>
                        </div>
                    </div>
            <?php } wp_reset_postdata();  ?>
          <p class="t-center no-margin"><a href="<?php echo site_url('/newsroom'); ?>" class="btn btn--yellow">Go to Newsroom</a></p>
        </div>
      </div>
    </div>

    <div class="hero-slider">
      <div data-glide-el="track" class="glide__track">
        <div class="glide__slides"><!-- slides container -->
          <div class="hero-slider__slide" style="background-image: url( <?php echo get_theme_file_uri('images/windows-JW-T2BH5k5E-unsplash.jpg'); ?> )">
            <div class="hero-slider__interior container">
              <div class="hero-slider__overlay">
                <h2 class="headline headline--medium t-center">10+ years curricula development</h2>
                <p class="t-center">Lorem ipsum dolor sit amet.</p>
                <p class="t-center no-margin"><a href="#" class="btn btn--blue">Learn more</a></p>
              </div>
            </div>
          </div><!-- end of slide #1 -->
          <div class="hero-slider__slide" style="background-image: url( <?php echo get_theme_file_uri('images/ux-store-jJT2r2n7lYA-unsplash.jpg'); ?> )">
            <div class="hero-slider__interior container">
              <div class="hero-slider__overlay">
                <h2 class="headline headline--medium t-center">75% students employment rate</h2>
                <p class="t-center">Lorem ipsum dolor sit amet.</p>
                <p class="t-center no-margin"><a href="#" class="btn btn--blue">Learn more</a></p>
              </div>
            </div>
          </div><!-- end of slide #2 -->
          <div class="hero-slider__slide" style="background-image: url( <?php echo get_theme_file_uri('images/jason-goodman-Oalh2MojUuk-unsplash.jpg'); ?> )">
            <div class="hero-slider__interior container">
              <div class="hero-slider__overlay">
                <h2 class="headline headline--medium t-center">1000+ enrolled students</h2>
                <p class="t-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus, illum?</p>
                <p class="t-center no-margin"><a href="#" class="btn btn--blue">Learn more</a></p>
              </div>
            </div>
          </div><!-- end of slide #3 -->
        </div> <!-- end of slides container -->
        <div class="slider__bullets glide__bullets" data-glide-el="controls[nav]"></div>
      </div>
    </div>

<?php get_footer(); ?>

